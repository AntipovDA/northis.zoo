﻿using System;
using Domain.Zoo;

namespace Domain.Food
{
	/// <summary>
	/// Представляет базовый класс для различных типов еды.
	/// </summary>
	public abstract class Food
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект типа <see cref="Food" /> с параметрами.
		/// </summary>
		/// <param name="calories">Относительное количество полезных веществ.</param>
		/// <param name="mass">Количество данного продукта.</param>
		protected Food(int mass){}
		#endregion

		#region Properties
		/// <summary>
		/// Получает содержание полезных веществ в типе еды.
		/// </summary>
		protected abstract Calorage FoodCalorage
		{
			get;
		}

		/// <summary>
		/// Получает количество данного продукта.
		/// </summary>
		protected abstract int Mass
		{
			get;
		}
		#endregion

		#region Overridable
		/// <summary>
		/// Расcчитывает количество полезных веществ еде.
		/// </summary>
		/// <returns>Количество полезных веществ.</returns>
		public Calorage CalculateCalories()
		{
			return new Calorage(
			    FoodCalorage.Calories * 10 * Mass,
			    FoodCalorage.Carbohydrates * 10 * Mass,
			    FoodCalorage.Fats * 10 * Mass,
			    FoodCalorage.Proteins * 10 * Mass
                );
		}
		#endregion
	}
}
