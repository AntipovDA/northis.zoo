﻿using Domain.Zoo;

namespace Domain.Food.MeatFood
{
	/// <summary>
	/// Представляет базовый тип Мясосодержащие продукты.
	/// </summary>
	internal abstract class MeatFood : Food
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект типа <see cref="MeatFood" /> с параметрами.
		/// </summary>
		/// <param name="mass">Количество данного продукта.</param>
		protected MeatFood(int mass)
			: base(mass)
		{
		}
		#endregion
	}
}
