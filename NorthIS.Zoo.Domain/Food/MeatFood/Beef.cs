﻿using System;
using Domain.Zoo;

namespace Domain.Food.MeatFood
{
	/// <summary>
	/// Представляет класс Говядина.
	/// </summary>
	internal class Beef : MeatFood
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект типа <see cref="Beef" /> с параметрами.
		/// </summary>
		/// <param name="mass">Количество данного продукта.</param>
		public Beef(int mass)
			: base(mass)
		{
		    if (mass <= 0 || mass >= 200)
		    {
		        throw new ArgumentException("Масса должна быть больше нуля и меньше 100 кг.");
		    }

            FoodCalorage = new Calorage(106, 0, 3, 21);
			Mass = mass;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Получает содержание полезных веществ в типе еды.
		/// </summary>
		protected override Calorage FoodCalorage
		{
			get;
		}

		/// <summary>
		/// Получает количество данного продукта.
		/// </summary>
		protected override int Mass
		{
			get;
		}
		#endregion
	}
}
