﻿using System;
using Domain.Zoo;

namespace Domain.Food.MeatFood
{
	/// <summary>
	/// Представляет класс Рыба (в качестве пищи).
	/// </summary>
	internal class Fish : MeatFood
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект типа <see cref="Fish" /> с параметрами.
		/// </summary>
		/// <param name="mass">Количество данного продукта.</param>
		public Fish(int mass)
			: base(mass)
		{
		    if (mass <= 0 || mass >= 200)
		    {
		        throw new ArgumentException("Масса должна быть больше нуля и меньше 100 кг.");
		    }

            FoodCalorage = new Calorage(134, 0, 5, 19);
			Mass = mass;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Получает содержание полезных веществ в типе еды.
		/// </summary>
		protected override Calorage FoodCalorage
		{
			get;
		}

		/// <summary>
		/// Получает количество данного продукта.
		/// </summary>
		protected override int Mass
		{
			get;
		}
		#endregion
	}
}
