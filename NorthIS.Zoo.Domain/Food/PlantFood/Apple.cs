﻿using System;
using System.CodeDom;
using Domain.Zoo;

namespace Domain.Food.PlantFood
{
	/// <summary>
	/// Представляет класс Яблоко.
	/// </summary>
	internal class Apple : PlantFood
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект типа <see cref="Apple" /> с параметрами.
		/// </summary>
		/// <param name="mass">Количество данного продукта.</param>
		public Apple(int mass)
			: base(mass)
		{
		    if (mass <= 0 || mass >= 200)
		    {
                throw new ArgumentException("Масса яблок должна быть больше нуля и меньше 100 кг.");
            } 

		    FoodCalorage = new Calorage(47, 10, 1, 1);
		    Mass = mass;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Получает содержание полезных веществ в типе еды.
		/// </summary>
		protected override Calorage FoodCalorage
		{
			get;
		}

		/// <summary>
		/// Получает количество данного продукта.
		/// </summary>
		protected override int Mass
		{
			get;
		}
		#endregion
	}
}
