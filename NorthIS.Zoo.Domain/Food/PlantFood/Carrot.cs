﻿using System;
using Domain.Zoo;

namespace Domain.Food.PlantFood
{
	/// <summary>
	/// Представляет класс Морковь.
	/// </summary>
	internal class Carrot : PlantFood
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект типа <see cref="Carrot" /> с параметрами.
		/// </summary>
		/// <param name="calories">Относительное количество полезных веществ.</param>
		/// <param name="mass">Количество данного продукта.</param>
		public Carrot(int mass)
			: base(mass)
		{
		    if (mass <= 0 || mass >= 200)
		    {
		        throw new ArgumentException("Масса моркови должна быть больше нуля и меньше 100 кг.");
		    }

            FoodCalorage = new Calorage(35, 7, 0, 1);
			Mass = mass;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Получает содержание полезных веществ в типе еды.
		/// </summary>
		protected override Calorage FoodCalorage
		{
			get;
		}

		/// <summary>
		/// Получает количество данного продукта.
		/// </summary>
		protected override int Mass
		{
			get;
		}
		#endregion
	}
}
