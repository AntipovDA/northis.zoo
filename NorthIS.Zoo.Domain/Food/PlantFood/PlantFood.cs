﻿using Domain.Zoo;

namespace Domain.Food.PlantFood
{
	/// <summary>
	/// Представляет базовый тип Растительные продукты.
	/// </summary>
	internal abstract class PlantFood : Food
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект типа <see cref="PlantFood" /> с параметрами.
		/// </summary>
		/// <param name="mass">Количество данного продукта.</param>
		public PlantFood(int mass)
			: base(mass)
		{
		}
		#endregion
	}
}
