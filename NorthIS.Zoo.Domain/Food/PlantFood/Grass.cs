﻿using System;
using Domain.Zoo;

namespace Domain.Food.PlantFood
{
	/// <summary>
	/// Представляет базовый тип Растительная Пища.
	/// </summary>
	internal class Grass : PlantFood
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект типа <see cref="Grass" /> с параметрами.
		/// </summary>
		/// <param name="mass">Количество данного продукта.</param>
		public Grass(int mass)
			: base(mass)
		{
		    if (mass <= 0 || mass >= 200)
		    {
		        throw new ArgumentException("Масса должна быть больше нуля и меньше 100 кг.");
		    }

            FoodCalorage = new Calorage(83, 37, 4, 13);
			Mass = mass;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Получает содержание полезных веществ в типе еды.
		/// </summary>
		protected override Calorage FoodCalorage
		{
			get;
		}

		/// <summary>
		/// Получает количество данного продукта.
		/// </summary>
		protected override int Mass
		{
			get;
		}
		#endregion
	}
}
