﻿using System;
using Domain.Animals;
using Domain.Animals.Birds;
using Domain.Animals.Mammals;
using Domain.Animals.Reptiles;

namespace Domain.Factory
{
	/// <summary>
	/// Инициализирует объекты различных видов животных.
	/// </summary>
	internal class AnimalFactory
	{
		#region Public
		/// <summary>
		/// Инициализирует объекты классов животных, соответствующие типу.
		/// </summary>
		/// <param name="animalType">Тип животного.</param>
		/// <param name="parameters">Параметры животного.</param>
		/// <param name="description">Параметры животного.</param>
		/// <returns>Объект животного заданного типа.</returns>
		internal Animal CreateAnimal(AnimalTypes animalType, AnimalParameters parameters, string description)
		{
			switch (animalType)
			{
				case AnimalTypes.Crocodile:
					return new Crocodile(parameters, description);
				case AnimalTypes.Python:
					return new Python(parameters, description);
				case AnimalTypes.Varan:
					return new Varan(parameters, description);
				case AnimalTypes.Eagle:
					return new Eagle(parameters, description);
				case AnimalTypes.Swan:
					return new Swan(parameters, description);
				case AnimalTypes.Duck:
					return new Duck(parameters, description);
				case AnimalTypes.Lion:
					return new Lion(parameters, description);
				case AnimalTypes.Bear:
					return new Bear(parameters, description);
				case AnimalTypes.Rabbit:
					return new Rabbit(parameters, description);
				case AnimalTypes.Antelope:
					return new Antelope(parameters, description);
				default:
					throw new NotImplementedException();
			}
		}
		#endregion
	}
}
