﻿using System;

namespace Domain.Animals
{
	/// <summary>
	/// Содержит в себе параметры животных.
	/// </summary>
	public class AnimalParameters
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект типа <see cref="AnimalParameters" /> с параметрами.
		/// </summary>
		/// <param name="age">Возраст животного.</param>
		/// <param name="name">Имя животного.</param>
		/// <param name="size">Размеры животного.</param>
		/// <param name="weight">Вес животного.</param>
		public AnimalParameters(int age, string name, int size, int weight)
		{
            Age = age;
			Name = name;
			Size = size;
			Weight = weight;
		}
        #endregion
        
	    #region Properties
        /// <summary>
        /// Получает возраст животного.
        /// </summary>
        public int Age
		{
			get;
		}

		/// <summary>
		/// Получает имя животного.
		/// </summary>
		public string Name
		{
			get;
		}

		/// <summary>
		/// Получает размер животного.
		/// </summary>
		public int Size
		{
			get;
		}

		/// <summary>
		/// Получает вес животного.
		/// </summary>
		public int Weight
		{
			get;
		}
		#endregion
	}
}
