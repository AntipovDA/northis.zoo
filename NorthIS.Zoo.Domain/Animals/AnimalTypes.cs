﻿namespace Domain.Animals
{
    /// <summary>
    /// Содержит виды животных.
    /// </summary>
    public enum AnimalTypes
    {
        /// <summary>
        /// Рептилии.
        /// </summary>
        Crocodile,
        Varan,
        Python,
        /// <summary>
        /// Птицы.
        /// </summary>
        Swan,
        Eagle,
        Duck,
        /// <summary>
        /// Млекопитающие.
        /// </summary>
        Bear,
        Rabbit,
        Lion,
        Antelope
    }
}