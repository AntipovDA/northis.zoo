﻿using System;
using System.Collections.Generic;
using Domain.Food;
using Domain.Zoo;

namespace Domain.Animals
{
	/// <summary>
	/// Представляет базовый тип Животное.
	/// </summary>
	internal abstract class Animal
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект класса <see cref="Animal" /> c параметрами.
		/// </summary>
		/// <param name="parameters">Основные параметры животного в виде объекта типа <see cref="AnimalParameters" />.</param>
		/// <param name="description">Описание конкретного животного.</param>
		protected Animal(AnimalParameters parameters, string description)
		{
		}
		#endregion

		#region Properties
		/// <summary>
		/// Получает или устанавливает количество полезных веществ, при котором животное считается сытым.
		/// </summary>
		public abstract Calorage CaloriesNeed
		{
			get;
			protected set;
		}

		/// <summary>
		/// Получает описание животного.
		/// </summary>
		public abstract string Description
		{
			get;
		}

		/// <summary>
		/// Получает параметры животного.
		/// </summary>
		public abstract AnimalParameters Parameters
		{
			get;
		}

		/// <summary>
		/// Получает или устанавливает рацион животного, которым оно может питаться.
		/// </summary>
		public abstract List<Type> Ration
		{
			get;
			protected set;
		}

		/// <summary>
		/// Получает или устанавливает текущее состояние насыщенности животного.
		/// </summary>
		internal abstract Calorage CaloriesCurrent
		{
			get;
			set;
		}
		#endregion

		#region Public
		/// <summary>
		/// Сбрасывает значение насыщенности животного.
		/// </summary>
		public void BecameHungry()
		{
			CaloriesCurrent = new Calorage(0, 0, 0, 0);
		}

		/// TODO:Реализовать реальный алгоритм.
		/// <summary>
		/// Увеличивает насыщенность животного, съевшего <see cref="Food" />.
		/// </summary>
		/// <param name="food">Еда для животного.</param>
		public void FoodEat(Food.Food food)
		{
			if (Ration.Contains(food.GetType()))
			{
				CaloriesCurrent = CaloriesCurrent + food.CalculateCalories();
			}
			else
			{
				throw new ArgumentException("Переданный тип еды не входит в рацион.");
			}
		}

		/// <summary>
		/// Получает имя животного.
		/// </summary>
		/// <returns>Строка, содержащая имя животного.</returns>
		public string GetName() => Parameters.Name;
		#endregion
	}
}
