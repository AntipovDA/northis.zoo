﻿namespace Domain.Animals.Mammals
{
	/// <summary>
	/// Представляет базовый тип Млекопитающее.
	/// </summary>
	internal abstract class Mammal : Animal
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект класса <see cref="Mammal" /> c параметрами.
		/// </summary>
		/// <param name="parameters">Основные параметры животного в виде объекта типа <see cref="AnimalParameters" />.</param>
		/// <param name="description">Описание конкретного животного.</param>
		protected Mammal(AnimalParameters parameters, string description)
			: base(parameters, description)
		{
		}
		#endregion
	}
}
