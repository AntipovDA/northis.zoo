﻿using System;
using System.Collections.Generic;
using Domain.Food.PlantFood;
using Domain.Zoo;

namespace Domain.Animals.Mammals
{
	/// <summary>
	/// Представляет класс Кролик.
	/// </summary>
	internal class Rabbit : Mammal
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект класса <see cref="Rabbit" /> c параметрами.
		/// </summary>
		/// <param name="parameters">Основные параметры животного в виде объекта типа <see cref="AnimalParameters" />.</param>
		/// <param name="description">Описание конкретного животного.</param>
		internal Rabbit(AnimalParameters parameters, string description)
			: base(parameters, description)
		{
			Parameters = parameters;
			Description = description;
			Ration = new List<Type>
			{
				typeof(Apple),
				typeof(Carrot),
				typeof(Grass)
			};
			CaloriesCurrent = new Calorage(0, 0, 0, 0);
			CaloriesNeed = new Calorage(1000, 500, 500, 200);
		}
		#endregion

		#region Properties
		/// <summary>
		/// Получает или устанавливает количество полезных веществ, при котором животное считается сытым.
		/// </summary>
		public override Calorage CaloriesNeed
		{
			get;
			protected set;
		}

		/// <summary>
		/// Получает описание животного.
		/// </summary>
		public override string Description
		{
			get;
		}

		/// <summary>
		/// Получает параметры животного.
		/// </summary>
		public override AnimalParameters Parameters
		{
			get;
		}

		/// <summary>
		/// Получает или устанавливает рацион животного, которым оно может питаться.
		/// </summary>
		public override List<Type> Ration
		{
			get;
			protected set;
		}

		/// <summary>
		/// Получает или устанавливает текущее состояние насыщенности животного.
		/// </summary>
		internal override Calorage CaloriesCurrent
		{
			get;
			set;
		}
		#endregion
	}
}
