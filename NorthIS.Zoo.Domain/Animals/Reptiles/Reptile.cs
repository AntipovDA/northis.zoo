﻿namespace Domain.Animals.Reptiles
{
	/// <summary>
	/// Представляет базовый тип Рептилия.
	/// </summary>
	internal abstract class Reptile : Animal
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект класса <see cref="Reptile" /> c параметрами.
		/// </summary>
		/// <param name="parameters">Основные параметры животного в виде объекта типа <see cref="AnimalParameters" />.</param>
		/// <param name="description">Описание конкретного животного.</param>
		protected Reptile(AnimalParameters parameters, string description)
			: base(parameters, description)
		{
		}
		#endregion
	}
}
