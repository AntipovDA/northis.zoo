﻿using System;
using System.Collections.Generic;
using Domain.Food.MeatFood;
using Domain.Zoo;

namespace Domain.Animals.Birds
{
	/// <summary>
	/// Представляет класс Орёл.
	/// </summary>
	internal class Eagle : Bird, IPredator
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект класса <see cref="Eagle" /> c параметрами.
		/// </summary>
		/// <param name="parameters">Основные параметры животного в виде объекта типа <see cref="AnimalParameters" />.</param>
		/// <param name="description">Описание конкретного животного.</param>
		internal Eagle(AnimalParameters parameters, string description)
			: base(parameters, description)
		{
			Parameters = parameters;
			Description = description;
			Ration = new List<Type>
			{
				typeof(Chicken),
				typeof(Fish)
			};
			CaloriesCurrent = new Calorage(0, 0, 0, 0);
			CaloriesNeed = new Calorage(2000, 1000, 1000, 1000);
		}
		#endregion

		#region Properties
		/// <summary>
		/// Получает или устанавливает количество полезных веществ, при котором животное считается сытым.
		/// </summary>
		public override Calorage CaloriesNeed
		{
			get;
			protected set;
		}

		/// <summary>
		/// Получает описание животного
		/// </summary>
		public override string Description
		{
			get;
		}

		/// <summary>
		/// Получает параметры животного.
		/// </summary>
		public override AnimalParameters Parameters
		{
			get;
		}

		/// <summary>
		/// Получает или устанавливает рацион животного, которым оно может питаться.
		/// </summary>
		public override List<Type> Ration
		{
			get;
			protected set;
		}

		/// <summary>
		/// Получает или устанавливает текущее состояние насыщенности животного.
		/// </summary>
		internal override Calorage CaloriesCurrent
		{
			get;
			set;
		}
		#endregion
        
	}
}
