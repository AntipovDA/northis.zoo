﻿namespace Domain.Animals.Birds
{
	/// <summary>
	/// Представляет базовый тип Птица.
	/// </summary>
	internal abstract class Bird : Animal
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект класса <see cref="Bird" /> c параметрами.
		/// </summary>
		/// <param name="parameters">Основные параметры животного в виде объекта типа <see cref="AnimalParameters" />.</param>
		/// <param name="description">Описание конкретного животного.</param>
		protected Bird(AnimalParameters parameters, string description)
			: base(parameters, description)
		{
		}
		#endregion
	}
}
