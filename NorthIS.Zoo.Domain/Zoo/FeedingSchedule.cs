﻿using System;
using System.Collections.Generic;

namespace Domain.Zoo
{
	/// <summary>
	/// Содержит функционал расписания для кормления животных в клетке.
	/// </summary>
	internal class FeedingSchedule
	{
		#region .ctor
		/// TODO: Написать тело конструктора.
		/// <summary>
		/// Инициализирует объект типа <see cref="FeedingSchedule" /> с параметрами.
		/// </summary>
		/// <param name="timeList">Список времени кормления.</param>
		/// <param name="cage">Клетка, к которой относится расписание.</param>
		private FeedingSchedule(List<DateTime> timeList, Cage cage) => throw new NotImplementedException();
		#endregion

		#region Properties
		/// <summary>
		/// Получает или устанавливает клетку, к которой привязано расписание.
		/// </summary>
		private Cage CageScheduled
		{
			get;
			set;
		}

		/// <summary>
		/// Получает список времени кормежки для клетки.
		/// </summary>
		private List<DateTime> TimeList
		{
			get;
		}
		#endregion

		#region Public
		/// TODO: Реализовать метод.
		/// <summary>
		/// Добавляет время в расписание.
		/// </summary>
		/// <param name="feedingTime">Время, которое нужно добавить.</param>
		public void AddTimeToSchedule(DateTime feedingTime)
		{
			throw new NotImplementedException();
		}

		/// TODO: Реализовать метод.
		/// <summary>
		/// Рассчитывает количество полезных веществ, необходимых для насыщения животных в клетке.
		/// </summary>
		public void CalculateNecessaryCalorage()
		{
			throw new NotImplementedException();
		}

		/// TODO: Реализовать метод.
		/// <summary>
		/// Убирает время из расписания.
		/// </summary>
		/// <param name="feedingTime">Время, которое необходимо убрать.</param>
		public void RemoveTimeFromSchedule(DateTime feedingTime)
		{
			throw new NotImplementedException();
		}
		#endregion
	}
}
