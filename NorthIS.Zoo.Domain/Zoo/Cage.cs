﻿using System;
using System.Collections.Generic;
using Domain.Animals;
using Domain.Factory;

namespace Domain.Zoo
{
	/// <summary>
	/// Содержит функционал клетки зоопарка.
	/// </summary>
	internal class Cage
	{
		#region Data
		#region Fields
		/// <summary>
		/// Определяет возможность добавления травоядных в клетку.
		/// </summary>
		private bool _canAddPlantEaters;
		#endregion
		#endregion

		#region .ctor
		/// <summary>
		/// Инициализирует новый объект типа Cage с параметрами.
		/// </summary>
		/// <param name="name">Название клетки.</param>
		/// <param name="maxweight"></param>
		/// <param name="types"></param>
		public Cage(string name, int maxweight, List<AnimalTypes> types)
		{
		}
		#endregion

		#region Properties
		/// <summary>
		/// Получает список допустимых видов животных для содержания в клетке.
		/// </summary>
		public List<AnimalTypes> AllowedAnimalTypes
		{
			get;
		}

		/// <summary>
		/// Получает список животных в клетке.
		/// </summary>
		public List<Animal> AnimalList
		{
			get;
		}

		/// <summary>
		/// Получает название клетки (идентификационный код?).
		/// </summary>
		public string CageName
		{
			get;
		}

		/// <summary>
		/// Получает максимальный суммарный вес животных в клетке (или максимальный вес животного?).
		/// </summary>
		public int MaxAnimalWeight
		{
			get;
		}

		/// <summary>
		/// Получает или устанавливает расписание кормления для клетки.
		/// </summary>
		public FeedingSchedule Schedule
		{
			get;
			protected set;
		}
		#endregion

		#region Public
		/// TODO:Реализовать реальный алгоритм.
		/// <summary>
		/// Добавляет животное в клетку.
		/// </summary>
		/// <param name="animal">Животное для добавления.</param>
		public void AddAnimal(Animal animal)
		{
			throw new NotImplementedException();
		}

		/// TODO:Реализовать реальный алгоритм.
		/// <summary>
		/// Кормит животных в клетке.
		/// </summary>
		/// <param name="food">Список еды.</param>
		public void FeedAnimals(List<Food.Food> food)
		{
			throw new NotImplementedException();
		}

		/// TODO:Реализовать реальный алгоритм.
		/// <summary>
		/// Удаляет животное из клетки.
		/// </summary>
		/// <param name="animal">Животное для удаления</param>
		public void RemoveAnimal(Animal animal)
		{
			throw new NotImplementedException();
		}
		#endregion

		#region Private
		/// TODO:Реализовать реальный алгоритм.
		/// <summary>
		/// Проверяет, может ли животное питаться заданной пищей.
		/// </summary>
		/// <param name="animal">Животное.</param>
		/// <param name="food">Еда.</param>
		/// <returns>true, если может, иначе false.</returns>
		private bool ValidateFeed(Animal animal, Food.Food food) => throw new NotImplementedException();
		#endregion
	}
}
