﻿using System.Collections.Generic;
using Domain.Animals;

namespace Domain.Zoo
{
	/// <summary>
	/// Предоставляет функционал для обновления данных животных.
	/// </summary>
	internal class DayManager
	{
		#region Private
		/// <summary>
		/// Обнуляет насыщенность животных из списка.
		/// </summary>
		/// <param name="animals">Список животных, насыщенность которых необходимо обнулить.</param>
		private void MakeHungry(List<Animal> animals)
		{
			foreach (var animalSelected in animals)
			{
				animalSelected.BecameHungry();
			}
		}
		#endregion
	}
}
