﻿namespace Domain.Zoo
{
	/// <summary>
	/// Содержит функционал полезных веществ, применяемый в других типах.
	/// </summary>
	public class Calorage
	{
		#region .ctor
		/// <summary>
		/// Инициализирует новый объект типа <see cref="Calorage" /> с параметрами.
		/// </summary>
		/// <param name="calories">Количество калорий на единицу.</param>
		/// <param name="carbs">Количество углеводов на единицу.</param>
		/// <param name="fats">Количество жиров на единицу.</param>
		/// <param name="proteins">Количество протеинов на единицу.</param>
		public Calorage(int calories, int carbs, int fats, int proteins)
		{
			Calories = calories;
			Carbohydrates = carbs;
			Fats = fats;
			Proteins = proteins;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Получает количество калорий.
		/// </summary>
		internal int Calories
		{
			get;
		}

		/// <summary>
		/// Получает количество углеводов.
		/// </summary>
		internal int Carbohydrates
		{
			get;
		}

		/// <summary>
		/// Получает количество жиров.
		/// </summary>
		internal int Fats
		{
			get;
		}

		/// <summary>
		/// Получает количество белков.
		/// </summary>
		internal int Proteins
		{
			get;
		}
		#endregion

		#region Operators
		public static Calorage operator +(Calorage c1, Calorage c2) =>
			new Calorage(c1.Calories + c2.Calories, c1.Carbohydrates + c2.Carbohydrates, c1.Fats + c2.Fats, c1.Proteins + c2.Proteins);
		#endregion
	}
}
