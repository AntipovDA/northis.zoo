﻿using System;
using System.Collections.Generic;
using Domain.Animals;

namespace Domain.Zoo
{
	/// <summary>
	/// Предоставляет функционал для работы зоопарка.
	/// </summary>
	internal class Zoo
	{
		#region Properties
		/// <summary>
		/// Получает список всех животных зоопарка.
		/// </summary>
		private List<Animal> AnimalList
		{
			get;
		}

		/// <summary>
		/// Получает список всех клеток зоопарка.
		/// </summary>
		private List<Cage> CageList
		{
			get;
		}

		/// <summary>
		/// Получает список всей еды зоопарка.
		/// </summary>
		private List<Food.Food> FoodList
		{
			get;
		}
		#endregion

		#region Private
		/// TODO: Реализовать метод.
		/// <summary>
		/// Добавляет животное в зоопарк.
		/// </summary>
		/// <param name="animal"></param>
		private void AddAnimal(Animal animal)
		{
			throw new NotImplementedException();
		}

		/// TODO: Реализовать метод.
		/// <summary>
		/// Добавляет клетку в зоопарк.
		/// </summary>
		/// <param name="cage"></param>
		private void AddCage(Cage cage)
		{
			throw new NotImplementedException();
		}

		/// TODO: Реализовать метод.
		/// <summary>
		/// Добавляет еду в зоопарк.
		/// </summary>
		/// <param name="food"></param>
		private void AddFood(Food.Food food)
		{
			throw new NotImplementedException();
		}

		/// TODO: Реализовать метод.
		/// <summary>
		/// Убирает животное из зоопарка.
		/// </summary>
		/// <param name="animal"></param>
		private void RemoveAnimal(Animal animal)
		{
			throw new NotImplementedException();
		}

		/// TODO: Реализовать метод.
		/// <summary>
		/// Убирает клетку из зоопарка.
		/// </summary>
		/// <param name="cage"></param>
		private void RemoveCage(Cage cage)
		{
			throw new NotImplementedException();
		}
		#endregion
	}
}
