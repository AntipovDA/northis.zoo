﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using Domain.Animals;
using Domain.Food;
using Domain.Food.MeatFood;
using Domain.Food.PlantFood;
using Domain.Zoo;
using NUnit.Framework;

namespace NorthIS.Zoo.Domain.Test.FoodTests
{
    [TestFixture]
    public class FoodTest
    {

        #region Data
        #region Static

        private static readonly object[] Mass =
        {
            -10,
            99999,
            -4873,
            0
        };

        private static readonly object[] Foods =
        {
            new Apple(34),
            new Carrot(4),
            new Grain(10),
            new Grass(46),
            new Beef(33),
            new Chicken(10),
            new Fish(47)
        };

        private static readonly object[] ResultCalorage =
        {
            new Calorage(15980, 3400, 340, 340),
            new Calorage(1400, 280, 0, 40),
            new Calorage(34200, 6700, 300, 1200),
            new Calorage(38180, 17020, 1840, 5980),
            new Calorage(34980, 0, 990, 6930),
            new Calorage(20000, 0, 1400, 1700),
            new Calorage(62980, 0, 2350, 8930)

        };

        #endregion
        #endregion

        [Test]
        [Sequential]
        [Description("Проверка подсчета энергетических вещест, на заданную массу определенных типов еды")]
        public void CalculateCalories_Is_Correct([ValueSource(nameof(Foods))] Food food, [ValueSource(nameof(ResultCalorage))] Calorage result)
        {
            Calorage calorage = food.CalculateCalories();
            Assert.That(calorage.Proteins, Is.EqualTo(result.Proteins));
            Assert.That(calorage.Calories, Is.EqualTo(result.Calories));
            Assert.That(calorage.Fats, Is.EqualTo(result.Fats));
            Assert.That(calorage.Carbohydrates, Is.EqualTo(result.Carbohydrates));
        }

        [Test]
        [TestCaseSource("Mass")]
        [Description("Проверка валидации при создании объекта еды.")]
        public void CheckExceptionForFoodsConstructors(int mass)
        {
            Assert.Throws<ArgumentException>(delegate
            {
                var food = new Apple(mass);
            });
            Assert.Throws<ArgumentException>(delegate
            {
                var food = new Carrot(mass);
            });
            Assert.Throws<ArgumentException>(delegate
            {
                var food = new Grain(mass);
            });
            Assert.Throws<ArgumentException>(delegate
            {
                var food = new Grass(mass);
            });
            Assert.Throws<ArgumentException>(delegate
            {
                var food = new Beef(mass);
            });
            Assert.Throws<ArgumentException>(delegate
            {
                var food = new Chicken(mass);
            });
            Assert.Throws<ArgumentException>(delegate
            {
                var food = new Fish(mass);
            });
        }
    }
}