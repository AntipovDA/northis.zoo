﻿using System;
using Domain.Animals;
using Domain.Animals.Birds;
using Domain.Food;
using Domain.Food.MeatFood;
using Domain.Food.PlantFood;
using NUnit.Framework;

namespace NorthIS.Zoo.Domain.Test.AnimalTests
{
	/// <summary>
	/// Проверка работы класса Duck.
	/// </summary>
	[TestFixture]
	internal class DuckTest
	{
		#region Data
		#region Static
		/// <summary>
		/// Различные параметры для инициализации объектов животных.
		/// </summary>
		private static readonly AnimalParameters Parameters1 = new AnimalParameters(0, "", 0, 0);
		private static readonly AnimalParameters Parameters2 = new AnimalParameters(10, "Abigale", 1000, 200);
		private static readonly AnimalParameters Parameters3 = new AnimalParameters(-90, "Bertram", 0, 120);
		private static readonly AnimalParameters Parameters4 = new AnimalParameters(35, "Ciri", -15, 250);
		private static readonly AnimalParameters Parameters5 = new AnimalParameters(0, "Damien", 21, -3);
		private static readonly AnimalParameters Parameters6 = new AnimalParameters(-1, "Edward", -1, -1);
		/// <summary>
		/// Источник различных экземпляров объектов Duck для тестов.
		/// </summary>
		private static readonly object[] Ducks =
		{
			new Duck(null, "Проверка с 'null' в параметрах"),
			new Duck(Parameters1, "Проверка с нулевыми параметрами"),
			new Duck(Parameters2, "Проверка на обычных параметрах"),
			new Duck(Parameters3, "Проверка на первый отрицательный параметр"),
			new Duck(Parameters4, "Проверка на третий отрицательный параметр"),
			new Duck(Parameters5, "Проверка на четвертый отрицательный параметр"),
			new Duck(Parameters6, "Проверка на все отрицательные числовые параметры")
		};
		/// <summary>
		/// Источник различных экземпляров объектов Food для тестов.
		/// </summary>
		private static readonly object[] FoodObjects =
		{
			new Apple(1),
			new Beef(1),
			new Carrot(1),
			new Chicken(1),
			new Fish(1),
			new Grain(1),
			new Grass(1)
		};
		#endregion

		#region Fields
		private readonly Type _referenceType = typeof(Duck);
		#endregion
		#endregion

		#region Public
		/// <summary>
		/// Проверяем инициализацию экземпляров класса <see cref="Duck" /> с различными видами параметров.
		/// </summary>
		/// <param name="duckTest"></param>
		[TestCaseSource(nameof(Ducks))]
		[Description("Проверка на корректность задания типа животного при инициализации")]
		public void Type_Is_Correct(Animal duckTest)
		{
			Assert.That(duckTest, Is.TypeOf(_referenceType));
		}

		[TestCaseSource(nameof(Ducks))]
		[Description("Проверка на создаваемость объектов класса Duck с разными параметрами")]
		public void Has_Values(Animal duckTest)
		{
			Assert.That(duckTest, Is.Not.Null);
		}

		[TestCaseSource(nameof(FoodObjects))]
		[Description("Проверка на корректность работы методов FoodEat и BecameHungry")]
		public void FoodEat_Is_Correct(Food foodTest)
		{
			Animal duckTest = new Duck(Parameters2, "Eats a lot");
			duckTest.FoodEat(foodTest);
			// Проверка на корректные задания значений после съедения.
			Assert.That(duckTest.CaloriesCurrent.Calories, Is.EqualTo(foodTest.CalculateCalories().Calories));
			Assert.That(duckTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(foodTest.CalculateCalories().Carbohydrates));
			Assert.That(duckTest.CaloriesCurrent.Fats, Is.EqualTo(foodTest.CalculateCalories().Fats));
			Assert.That(duckTest.CaloriesCurrent.Proteins, Is.EqualTo(foodTest.CalculateCalories().Proteins));
			// Проверка на то, что CaloriesCurrent задается не большим чем CaloriesNeed.
			Assert.That(duckTest.CaloriesCurrent.Calories, Is.LessThanOrEqualTo(duckTest.CaloriesNeed.Calories));
			Assert.That(duckTest.CaloriesCurrent.Carbohydrates, Is.LessThanOrEqualTo(duckTest.CaloriesNeed.Carbohydrates));
			Assert.That(duckTest.CaloriesCurrent.Fats, Is.LessThanOrEqualTo(duckTest.CaloriesNeed.Fats));
			Assert.That(duckTest.CaloriesCurrent.Proteins, Is.LessThanOrEqualTo(duckTest.CaloriesNeed.Proteins));
			// Проверка на корректное обнуление при вызове BecameHungry.
			duckTest.BecameHungry();
			Assert.That(duckTest.CaloriesCurrent.Calories, Is.EqualTo(0));
			Assert.That(duckTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(0));
			Assert.That(duckTest.CaloriesCurrent.Fats, Is.EqualTo(0));
			Assert.That(duckTest.CaloriesCurrent.Proteins, Is.EqualTo(0));
		}

		#endregion
	}
}
