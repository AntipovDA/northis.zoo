﻿using System;
using Domain.Animals;
using Domain.Animals.Reptiles;
using Domain.Food;
using Domain.Food.MeatFood;
using Domain.Food.PlantFood;
using NUnit.Framework;

namespace NorthIS.Zoo.Domain.Test.AnimalTests
{
    /// <summary>
    /// Проверка работы класса Python.
    /// </summary>
    [TestFixture]
    internal class PythonTest
    {
        #region Data
        #region Static

        /// <summary>
        /// Различные параметры для инициализации объектов животных.
        /// </summary>
        private static readonly object[] IncorrectParameters =
        {
            new AnimalParameters(0, "", 0, 0),
            new AnimalParameters(-324, "Bertram", 0, 1520),
            new AnimalParameters(35, "Ciri", -15, 250),
            new AnimalParameters(0, "Damien", 321, -4),
            new AnimalParameters(-1, "Edward", -1, -121),
            new AnimalParameters(10, "Cirilla", 1000, 200)
        };

        private static readonly object[] IncorrectDescriptions =
        {
            null,
            ""
        };

        private static readonly AnimalParameters CorrectParameters = new AnimalParameters(8, "Рики", 500, 120);

        /// <summary>
        /// Источник различных экземпляров объектов Food для тестов.
        /// </summary>
        private static readonly object[] FoodObjects =
        {
            new Apple(1),
            new Carrot(1),
            new Fish(1),
            new Grain(1),
            new Grass(1)
        };

        private static readonly object[] FoodFromRation =
        {
            new Beef(1),
            new Chicken(1)
        };
        #endregion
        
        #endregion

        #region Public

        [TestCaseSource(nameof(IncorrectParameters))]
        [Description("Проверка на исключение ситуации создания питона с не корректными параметрами")]
        public void Test_Exception_Create_Python_With_Incorrect_Parameters(AnimalParameters parameters)
        {
            Assert.Throws<ArgumentException>(delegate { var python = new Python(parameters, "Description"); });
        }


        [TestCaseSource(nameof(FoodObjects))]
        [Description("Проверка на исключение ситуации кормления питона едой не входящий в его рацион")]
        public void FoodEat_With_Food_Not_Enteing_In_Ration(Food food)
        {
            var python = new Python(CorrectParameters, "Description");
            var ex = Assert.Throws<ArgumentException>((() => python.FoodEat(food)));
            Assert.IsTrue(ex.Message.Contains("Переданный тип еды не входит в рацион."));
        }

        [TestCaseSource(nameof(FoodFromRation))]
        [Description("Проверка на корректность работы методов FoodEat и BecameHungry")]
        public void FoodEat_Whit_Food_From_Ration_Is_Correct(Food foodTest)
        {
            Animal pythonTest = new Python(CorrectParameters, "Eats a lot");
            pythonTest.FoodEat(foodTest);

            // Проверка на корректные задания значений после съедения.
            Assert.That(pythonTest.CaloriesCurrent.Calories, Is.EqualTo(foodTest.CalculateCalories().Calories));
            Assert.That(pythonTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(foodTest.CalculateCalories().Carbohydrates));
            Assert.That(pythonTest.CaloriesCurrent.Fats, Is.EqualTo(foodTest.CalculateCalories().Fats));
            Assert.That(pythonTest.CaloriesCurrent.Proteins, Is.EqualTo(foodTest.CalculateCalories().Proteins));

            // Проверка на то, что CaloriesCurrent задается не большим чем CaloriesNeed.
            Assert.That(pythonTest.CaloriesCurrent.Calories, Is.LessThanOrEqualTo(pythonTest.CaloriesNeed.Calories));
            Assert.That(pythonTest.CaloriesCurrent.Carbohydrates, Is.LessThanOrEqualTo(pythonTest.CaloriesNeed.Carbohydrates));
            Assert.That(pythonTest.CaloriesCurrent.Fats, Is.LessThanOrEqualTo(pythonTest.CaloriesNeed.Fats));
            Assert.That(pythonTest.CaloriesCurrent.Proteins, Is.LessThanOrEqualTo(pythonTest.CaloriesNeed.Proteins));

            // Проверка на корректное обнуление при вызове BecameHungry.
            pythonTest.BecameHungry();
            Assert.That(pythonTest.CaloriesCurrent.Calories, Is.EqualTo(0));
            Assert.That(pythonTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(0));
            Assert.That(pythonTest.CaloriesCurrent.Fats, Is.EqualTo(0));
            Assert.That(pythonTest.CaloriesCurrent.Proteins, Is.EqualTo(0));
        }

        #endregion
    }
}
