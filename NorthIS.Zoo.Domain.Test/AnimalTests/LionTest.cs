﻿using System;
using Domain.Animals;
using Domain.Animals.Mammals;
using Domain.Food;
using Domain.Food.MeatFood;
using Domain.Food.PlantFood;
using NUnit.Framework;

namespace NorthIS.Zoo.Domain.Test.AnimalTests
{
	/// <summary>
	/// Проверка работы класса Lion.
	/// </summary>
	[TestFixture]
	internal class LionTest
	{
		#region Data
		#region Static
		/// <summary>
		/// Различные параметры для инициализации объектов животных.
		/// </summary>
		private static readonly AnimalParameters Parameters1 = new AnimalParameters(0, "", 0, 0);
		private static readonly AnimalParameters Parameters2 = new AnimalParameters(10, "Abigale", 1000, 200);
		private static readonly AnimalParameters Parameters3 = new AnimalParameters(-90, "Bertram", 0, 120);
		private static readonly AnimalParameters Parameters4 = new AnimalParameters(35, "Ciri", -15, 250);
		private static readonly AnimalParameters Parameters5 = new AnimalParameters(0, "Damien", 21, -3);
		private static readonly AnimalParameters Parameters6 = new AnimalParameters(-1, "Edward", -1, -1);
		/// <summary>
		/// Источник различных экземпляров объектов Lion для тестов.
		/// </summary>
		private static readonly object[] Lions =
		{
			new Lion(null, "Проверка с 'null' в параметрах"),
			new Lion(Parameters1, "Проверка с нулевыми параметрами"),
			new Lion(Parameters2, "Проверка на обычных параметрах"),
			new Lion(Parameters3, "Проверка на первый отрицательный параметр"),
			new Lion(Parameters4, "Проверка на третий отрицательный параметр"),
			new Lion(Parameters5, "Проверка на четвертый отрицательный параметр"),
			new Lion(Parameters6, "Проверка на все отрицательные числовые параметры")
		};
		/// <summary>
		/// Источник различных экземпляров объектов Food для тестов.
		/// </summary>
		private static readonly object[] FoodObjects =
		{
			new Apple(1),
			new Beef(1),
			new Carrot(1),
			new Chicken(1),
			new Fish(1),
			new Grain(1),
			new Grass(1)
		};
		#endregion

		#region Fields
		private readonly Type _referenceType = typeof(Lion);
		#endregion
		#endregion

		#region Public
		/// <summary>
		/// Проверяем инициализацию экземпляров класса <see cref="Lion" /> с различными видами параметров.
		/// </summary>
		/// <param name="lionTest"></param>
		[TestCaseSource(nameof(Lions))]
		[Description("Проверка на корректность задания типа животного при инициализации")]
		public void Type_Is_Correct(Animal lionTest)
		{
			Assert.That(lionTest, Is.TypeOf(_referenceType));
		}

		[TestCaseSource(nameof(Lions))]
		[Description("Проверка на создаваемость объектов класса Lion с разными параметрами")]
		public void Has_Values(Animal lionTest)
		{
			Assert.That(lionTest, Is.Not.Null);
		}

		[TestCaseSource(nameof(FoodObjects))]
		[Description("Проверка на корректность работы методов FoodEat и BecameHungry")]
		public void FoodEat_Is_Correct(Food foodTest)
		{
			Animal lionTest = new Lion(Parameters2, "Eats a lot");
			lionTest.FoodEat(foodTest);
			// Проверка на корректные задания значений после съедения.
			Assert.That(lionTest.CaloriesCurrent.Calories, Is.EqualTo(foodTest.CalculateCalories().Calories));
			Assert.That(lionTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(foodTest.CalculateCalories().Carbohydrates));
			Assert.That(lionTest.CaloriesCurrent.Fats, Is.EqualTo(foodTest.CalculateCalories().Fats));
			Assert.That(lionTest.CaloriesCurrent.Proteins, Is.EqualTo(foodTest.CalculateCalories().Proteins));
			// Проверка на то, что CaloriesCurrent задается не большим чем CaloriesNeed.
			Assert.That(lionTest.CaloriesCurrent.Calories, Is.LessThanOrEqualTo(lionTest.CaloriesNeed.Calories));
			Assert.That(lionTest.CaloriesCurrent.Carbohydrates, Is.LessThanOrEqualTo(lionTest.CaloriesNeed.Carbohydrates));
			Assert.That(lionTest.CaloriesCurrent.Fats, Is.LessThanOrEqualTo(lionTest.CaloriesNeed.Fats));
			Assert.That(lionTest.CaloriesCurrent.Proteins, Is.LessThanOrEqualTo(lionTest.CaloriesNeed.Proteins));
			// Проверка на корректное обнуление при вызове BecameHungry.
			lionTest.BecameHungry();
			Assert.That(lionTest.CaloriesCurrent.Calories, Is.EqualTo(0));
			Assert.That(lionTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(0));
			Assert.That(lionTest.CaloriesCurrent.Fats, Is.EqualTo(0));
			Assert.That(lionTest.CaloriesCurrent.Proteins, Is.EqualTo(0));
		}

		#endregion
	}
}
