﻿using System;
using Domain.Animals;
using Domain.Animals.Mammals;
using Domain.Food;
using Domain.Food.MeatFood;
using Domain.Food.PlantFood;
using NUnit.Framework;

namespace NorthIS.Zoo.Domain.Test.AnimalTests
{
	/// <summary>
	/// Проверка работы класса Rabbit.
	/// </summary>
	[TestFixture]
	internal class RabbitTest
	{
		#region Data
		#region Static
		/// <summary>
		/// Различные параметры для инициализации объектов животных.
		/// </summary>
		private static readonly AnimalParameters Parameters1 = new AnimalParameters(0, "", 0, 0);
		private static readonly AnimalParameters Parameters2 = new AnimalParameters(10, "Abigale", 1000, 200);
		private static readonly AnimalParameters Parameters3 = new AnimalParameters(-90, "Bertram", 0, 120);
		private static readonly AnimalParameters Parameters4 = new AnimalParameters(35, "Ciri", -15, 250);
		private static readonly AnimalParameters Parameters5 = new AnimalParameters(0, "Damien", 21, -3);
		private static readonly AnimalParameters Parameters6 = new AnimalParameters(-1, "Edward", -1, -1);
		/// <summary>
		/// Источник различных экземпляров объектов Rabbit для тестов.
		/// </summary>
		private static readonly object[] Rabbits =
		{
			new Rabbit(null, "Проверка с 'null' в параметрах"),
			new Rabbit(Parameters1, "Проверка с нулевыми параметрами"),
			new Rabbit(Parameters2, "Проверка на обычных параметрах"),
			new Rabbit(Parameters3, "Проверка на первый отрицательный параметр"),
			new Rabbit(Parameters4, "Проверка на третий отрицательный параметр"),
			new Rabbit(Parameters5, "Проверка на четвертый отрицательный параметр"),
			new Rabbit(Parameters6, "Проверка на все отрицательные числовые параметры")
		};
		/// <summary>
		/// Источник различных экземпляров объектов Food для тестов.
		/// </summary>
		private static readonly object[] FoodObjects =
		{
			new Apple(1),
			new Beef(1),
			new Carrot(1),
			new Chicken(1),
			new Fish(1),
			new Grain(1),
			new Grass(1)
		};
		#endregion

		#region Fields
		private readonly Type _referenceType = typeof(Rabbit);
		#endregion
		#endregion

		#region Public
		/// <summary>
		/// Проверяем инициализацию экземпляров класса <see cref="Rabbit" /> с различными видами параметров.
		/// </summary>
		/// <param name="rabbitTest"></param>
		[TestCaseSource(nameof(Rabbits))]
		[Description("Проверка на корректность задания типа животного при инициализации")]
		public void Type_Is_Correct(Animal rabbitTest)
		{
			Assert.That(rabbitTest, Is.TypeOf(_referenceType));
		}

		[TestCaseSource(nameof(Rabbits))]
		[Description("Проверка на создаваемость объектов класса Rabbit с разными параметрами")]
		public void Has_Values(Animal rabbitTest)
		{
			Assert.That(rabbitTest, Is.Not.Null);
		}

		[TestCaseSource(nameof(FoodObjects))]
		[Description("Проверка на корректность работы методов FoodEat и BecameHungry")]
		public void FoodEat_Is_Correct(Food foodTest)
		{
			Animal rabbitTest = new Rabbit(Parameters2, "Eats a lot");
			rabbitTest.FoodEat(foodTest);
			// Проверка на корректные задания значений после съедения.
			Assert.That(rabbitTest.CaloriesCurrent.Calories, Is.EqualTo(foodTest.CalculateCalories().Calories));
			Assert.That(rabbitTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(foodTest.CalculateCalories().Carbohydrates));
			Assert.That(rabbitTest.CaloriesCurrent.Fats, Is.EqualTo(foodTest.CalculateCalories().Fats));
			Assert.That(rabbitTest.CaloriesCurrent.Proteins, Is.EqualTo(foodTest.CalculateCalories().Proteins));
			// Проверка на то, что CaloriesCurrent задается не большим чем CaloriesNeed.
			Assert.That(rabbitTest.CaloriesCurrent.Calories, Is.LessThanOrEqualTo(rabbitTest.CaloriesNeed.Calories));
			Assert.That(rabbitTest.CaloriesCurrent.Carbohydrates, Is.LessThanOrEqualTo(rabbitTest.CaloriesNeed.Carbohydrates));
			Assert.That(rabbitTest.CaloriesCurrent.Fats, Is.LessThanOrEqualTo(rabbitTest.CaloriesNeed.Fats));
			Assert.That(rabbitTest.CaloriesCurrent.Proteins, Is.LessThanOrEqualTo(rabbitTest.CaloriesNeed.Proteins));
			// Проверка на корректное обнуление при вызове BecameHungry.
			rabbitTest.BecameHungry();
			Assert.That(rabbitTest.CaloriesCurrent.Calories, Is.EqualTo(0));
			Assert.That(rabbitTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(0));
			Assert.That(rabbitTest.CaloriesCurrent.Fats, Is.EqualTo(0));
			Assert.That(rabbitTest.CaloriesCurrent.Proteins, Is.EqualTo(0));
		}

		#endregion
	}
}
