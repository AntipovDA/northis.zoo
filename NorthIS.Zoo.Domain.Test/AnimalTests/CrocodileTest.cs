﻿using System;
using Domain.Animals;
using Domain.Animals.Reptiles;
using Domain.Food;
using Domain.Food.MeatFood;
using Domain.Food.PlantFood;
using NUnit.Framework;

namespace NorthIS.Zoo.Domain.Test.AnimalTests
{
	/// <summary>
	/// Проверка работы класса Crocodile.
	/// </summary>
	[TestFixture]
	internal class CrocodileTest
	{
		#region Data
		#region Static
		/// <summary>
		/// Различные параметры для инициализации объектов животных.
		/// </summary>
		private static readonly AnimalParameters Parameters1 = new AnimalParameters(0, "", 0, 0);
		private static readonly AnimalParameters Parameters2 = new AnimalParameters(10, "Abigale", 1000, 200);
		private static readonly AnimalParameters Parameters3 = new AnimalParameters(-90, "Bertram", 0, 120);
		private static readonly AnimalParameters Parameters4 = new AnimalParameters(35, "Ciri", -15, 250);
		private static readonly AnimalParameters Parameters5 = new AnimalParameters(0, "Damien", 21, -3);
		private static readonly AnimalParameters Parameters6 = new AnimalParameters(-1, "Edward", -1, -1);
		/// <summary>
		/// Источник различных экземпляров объектов Crocodile для тестов.
		/// </summary>
		private static readonly object[] Crocodiles =
		{
			new Crocodile(null, "Проверка с 'null' в параметрах"),
			new Crocodile(Parameters1, "Проверка с нулевыми параметрами"),
			new Crocodile(Parameters2, "Проверка на обычных параметрах"),
			new Crocodile(Parameters3, "Проверка на первый отрицательный параметр"),
			new Crocodile(Parameters4, "Проверка на третий отрицательный параметр"),
			new Crocodile(Parameters5, "Проверка на четвертый отрицательный параметр"),
			new Crocodile(Parameters6, "Проверка на все отрицательные числовые параметры")
		};
		/// <summary>
		/// Источник различных экземпляров объектов Food для тестов.
		/// </summary>
		private static readonly object[] FoodObjects =
		{
			new Apple(1),
			new Beef(1),
			new Carrot(1),
			new Chicken(1),
			new Fish(1),
			new Grain(1),
			new Grass(1)
		};
		#endregion

		#region Fields
		private readonly Type _referenceType = typeof(Crocodile);
		#endregion
		#endregion

		#region Public
		/// <summary>
		/// Проверяем инициализацию экземпляров класса <see cref="Crocodile" /> с различными видами параметров.
		/// </summary>
		/// <param name="crocodileTest"></param>
		[TestCaseSource(nameof(Crocodiles))]
		[Description("Проверка на корректность задания типа животного при инициализации")]
		public void Type_Is_Correct(Animal crocodileTest)
		{
			Assert.That(crocodileTest, Is.TypeOf(_referenceType));
		}

		[TestCaseSource(nameof(Crocodiles))]
		[Description("Проверка на создаваемость объектов класса Crocodile с разными параметрами")]
		public void Has_Values(Animal crocodileTest)
		{
			Assert.That(crocodileTest, Is.Not.Null);
		}

		[TestCaseSource(nameof(FoodObjects))]
		[Description("Проверка на корректность работы методов FoodEat и BecameHungry")]
		public void FoodEat_Is_Correct(Food foodTest)
		{
			Animal crocodileTest = new Crocodile(Parameters2, "Eats a lot");
			crocodileTest.FoodEat(foodTest);
			// Проверка на корректные задания значений после съедения.
			Assert.That(crocodileTest.CaloriesCurrent.Calories, Is.EqualTo(foodTest.CalculateCalories().Calories));
			Assert.That(crocodileTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(foodTest.CalculateCalories().Carbohydrates));
			Assert.That(crocodileTest.CaloriesCurrent.Fats, Is.EqualTo(foodTest.CalculateCalories().Fats));
			Assert.That(crocodileTest.CaloriesCurrent.Proteins, Is.EqualTo(foodTest.CalculateCalories().Proteins));
			// Проверка на то, что CaloriesCurrent задается не большим чем CaloriesNeed.
			Assert.That(crocodileTest.CaloriesCurrent.Calories, Is.LessThanOrEqualTo(crocodileTest.CaloriesNeed.Calories));
			Assert.That(crocodileTest.CaloriesCurrent.Carbohydrates, Is.LessThanOrEqualTo(crocodileTest.CaloriesNeed.Carbohydrates));
			Assert.That(crocodileTest.CaloriesCurrent.Fats, Is.LessThanOrEqualTo(crocodileTest.CaloriesNeed.Fats));
			Assert.That(crocodileTest.CaloriesCurrent.Proteins, Is.LessThanOrEqualTo(crocodileTest.CaloriesNeed.Proteins));
			// Проверка на корректное обнуление при вызове BecameHungry.
			crocodileTest.BecameHungry();
			Assert.That(crocodileTest.CaloriesCurrent.Calories, Is.EqualTo(0));
			Assert.That(crocodileTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(0));
			Assert.That(crocodileTest.CaloriesCurrent.Fats, Is.EqualTo(0));
			Assert.That(crocodileTest.CaloriesCurrent.Proteins, Is.EqualTo(0));
		}

		#endregion
	}
}

