﻿using System;
using Domain.Animals;
using Domain.Animals.Reptiles;
using Domain.Food;
using Domain.Food.MeatFood;
using Domain.Food.PlantFood;
using NUnit.Framework;

namespace NorthIS.Zoo.Domain.Test.AnimalTests
{
	/// <summary>
	/// Проверка работы класса Varan.
	/// </summary>
	[TestFixture]
	internal class VaranTest
	{
        #region Data
        #region Static

	    /// <summary>
	    /// Различные параметры для инициализации объектов животных.
	    /// </summary>
	    private static readonly object[] IncorrectParameters =
	    {
	        new AnimalParameters(0, "", 0, 0),
	        new AnimalParameters(-90, "Bertram", 0, 120),
            new AnimalParameters(35, "Ciri", -15, 250),
	        new AnimalParameters(0, "Damien", 21, -3),
	        new AnimalParameters(-1, "Edward", -1, -1),
	        new AnimalParameters(10, "Cirilla", 1000, 200)
        };

	    private static readonly object[] IncorrectDescriptions =
	    {
	        null,
	        ""
	    };

	    private static readonly AnimalParameters CorrectParameters = new AnimalParameters(10, "Cirilla", 200, 90);

		/// <summary>
		/// Источник различных экземпляров объектов Food для тестов.
		/// </summary>
		private static readonly object[] FoodObjects =
		{
			new Apple(1),
			new Carrot(1),
			new Fish(1),
			new Grain(1),
			new Grass(1)
		};

	    private static readonly object[] FoodFromRation =
	    {
	        new Beef(1),
	        new Chicken(1)
	    };
        #endregion

        #region Fields
        private readonly Type _referenceType = typeof(Varan);
        #endregion
        #endregion

        #region Public

        [TestCaseSource(nameof(IncorrectParameters))]
        [Description("Проверка на исключение ситуации создания варана с не корректными параметрами")]
        public void Test_Exception_Create_Varan_With_Incorrect_Parameters(AnimalParameters parameters)
        {
            Assert.Throws<ArgumentException>(delegate { var varan = new Varan(parameters, "Description"); });
        }


	    [TestCaseSource(nameof(FoodObjects))]
	    [Description("Проверка на исключение ситуации кормления варана едой не входящий в его рацион")]
	    public void FoodEat_With_Food_Not_Enteing_In_Ration(Food food)
	    {
	        var varan = new Varan(CorrectParameters, "Description");
            var ex = Assert.Throws<ArgumentException>((() => varan.FoodEat(food)));
            Assert.IsTrue(ex.Message.Contains("Переданный тип еды не входит в рацион."));
	    }

        [TestCaseSource(nameof(FoodFromRation))]
		[Description("Проверка на корректность работы методов FoodEat и BecameHungry")]
		public void FoodEat_Whit_Food_From_Ration_Is_Correct(Food foodTest)
		{
			Animal varanTest = new Varan(CorrectParameters, "Eats a lot");
			varanTest.FoodEat(foodTest);

            // Проверка на корректные задания значений после съедения.
            Assert.That(varanTest.CaloriesCurrent.Calories, Is.EqualTo(foodTest.CalculateCalories().Calories));
            Assert.That(varanTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(foodTest.CalculateCalories().Carbohydrates));
            Assert.That(varanTest.CaloriesCurrent.Fats, Is.EqualTo(foodTest.CalculateCalories().Fats));
            Assert.That(varanTest.CaloriesCurrent.Proteins, Is.EqualTo(foodTest.CalculateCalories().Proteins));

            // Проверка на то, что CaloriesCurrent задается не большим чем CaloriesNeed.
            Assert.That(varanTest.CaloriesCurrent.Calories, Is.LessThanOrEqualTo(varanTest.CaloriesNeed.Calories));
            Assert.That(varanTest.CaloriesCurrent.Carbohydrates, Is.LessThanOrEqualTo(varanTest.CaloriesNeed.Carbohydrates));
            Assert.That(varanTest.CaloriesCurrent.Fats, Is.LessThanOrEqualTo(varanTest.CaloriesNeed.Fats));
            Assert.That(varanTest.CaloriesCurrent.Proteins, Is.LessThanOrEqualTo(varanTest.CaloriesNeed.Proteins));

            // Проверка на корректное обнуление при вызове BecameHungry.
            varanTest.BecameHungry();
            Assert.That(varanTest.CaloriesCurrent.Calories, Is.EqualTo(0));
            Assert.That(varanTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(0));
            Assert.That(varanTest.CaloriesCurrent.Fats, Is.EqualTo(0));
            Assert.That(varanTest.CaloriesCurrent.Proteins, Is.EqualTo(0));
        }

		#endregion
	}
}
