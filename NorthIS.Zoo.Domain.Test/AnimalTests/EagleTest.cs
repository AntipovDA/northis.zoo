﻿using System;
using Domain.Animals;
using Domain.Animals.Birds;
using Domain.Food;
using Domain.Food.MeatFood;
using Domain.Food.PlantFood;
using NUnit.Framework;

namespace NorthIS.Zoo.Domain.Test.AnimalTests
{
	/// <summary>
	/// Проверка работы класса Eagle.
	/// </summary>
	[TestFixture]
	internal class EagleTest
	{
		#region Data
		#region Static
		/// <summary>
		/// Различные параметры для инициализации объектов животных.
		/// </summary>
		private static readonly AnimalParameters Parameters1 = new AnimalParameters(0, "", 0, 0);
		private static readonly AnimalParameters Parameters2 = new AnimalParameters(10, "Abigale", 1000, 200);
		private static readonly AnimalParameters Parameters3 = new AnimalParameters(-90, "Bertram", 0, 120);
		private static readonly AnimalParameters Parameters4 = new AnimalParameters(35, "Ciri", -15, 250);
		private static readonly AnimalParameters Parameters5 = new AnimalParameters(0, "Damien", 21, -3);
		private static readonly AnimalParameters Parameters6 = new AnimalParameters(-1, "Edward", -1, -1);
		/// <summary>
		/// Источник различных экземпляров объектов Eagle для тестов.
		/// </summary>
		private static readonly object[] Eagles =
		{
			new Eagle(null, "Проверка с 'null' в параметрах"),
			new Eagle(Parameters1, "Проверка с нулевыми параметрами"),
			new Eagle(Parameters2, "Проверка на обычных параметрах"),
			new Eagle(Parameters3, "Проверка на первый отрицательный параметр"),
			new Eagle(Parameters4, "Проверка на третий отрицательный параметр"),
			new Eagle(Parameters5, "Проверка на четвертый отрицательный параметр"),
			new Eagle(Parameters6, "Проверка на все отрицательные числовые параметры")
		};
		/// <summary>
		/// Источник различных экземпляров объектов Food для тестов.
		/// </summary>
		private static readonly object[] FoodObjects =
		{
			new Apple(1),
			new Beef(1),
			new Carrot(1),
			new Chicken(1),
			new Fish(1),
			new Grain(1),
			new Grass(1)
		};
		#endregion

		#region Fields
		private readonly Type _referenceType = typeof(Eagle);
		#endregion
		#endregion

		#region Public
		/// <summary>
		/// Проверяем инициализацию экземпляров класса <see cref="Eagle" /> с различными видами параметров.
		/// </summary>
		/// <param name="eagleTest"></param>
		[TestCaseSource(nameof(Eagles))]
		[Description("Проверка на корректность задания типа животного при инициализации")]
		public void Type_Is_Correct(Animal eagleTest)
		{
			Assert.That(eagleTest, Is.TypeOf(_referenceType));
		}

		[TestCaseSource(nameof(Eagles))]
		[Description("Проверка на создаваемость объектов класса Eagle с разными параметрами")]
		public void Has_Values(Animal eagleTest)
		{
			Assert.That(eagleTest, Is.Not.Null);
		}

		[TestCaseSource(nameof(FoodObjects))]
		[Description("Проверка на корректность работы методов FoodEat и BecameHungry")]
		public void FoodEat_Is_Correct(Food foodTest)
		{
			Animal eagleTest = new Eagle(Parameters2, "Eats a lot");
			eagleTest.FoodEat(foodTest);
			// Проверка на корректные задания значений после съедения.
			Assert.That(eagleTest.CaloriesCurrent.Calories, Is.EqualTo(foodTest.CalculateCalories().Calories));
			Assert.That(eagleTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(foodTest.CalculateCalories().Carbohydrates));
			Assert.That(eagleTest.CaloriesCurrent.Fats, Is.EqualTo(foodTest.CalculateCalories().Fats));
			Assert.That(eagleTest.CaloriesCurrent.Proteins, Is.EqualTo(foodTest.CalculateCalories().Proteins));
			// Проверка на то, что CaloriesCurrent задается не большим чем CaloriesNeed.
			Assert.That(eagleTest.CaloriesCurrent.Calories, Is.LessThanOrEqualTo(eagleTest.CaloriesNeed.Calories));
			Assert.That(eagleTest.CaloriesCurrent.Carbohydrates, Is.LessThanOrEqualTo(eagleTest.CaloriesNeed.Carbohydrates));
			Assert.That(eagleTest.CaloriesCurrent.Fats, Is.LessThanOrEqualTo(eagleTest.CaloriesNeed.Fats));
			Assert.That(eagleTest.CaloriesCurrent.Proteins, Is.LessThanOrEqualTo(eagleTest.CaloriesNeed.Proteins));
			// Проверка на корректное обнуление при вызове BecameHungry.
			eagleTest.BecameHungry();
			Assert.That(eagleTest.CaloriesCurrent.Calories, Is.EqualTo(0));
			Assert.That(eagleTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(0));
			Assert.That(eagleTest.CaloriesCurrent.Fats, Is.EqualTo(0));
			Assert.That(eagleTest.CaloriesCurrent.Proteins, Is.EqualTo(0));
		}

		#endregion
	}
}
