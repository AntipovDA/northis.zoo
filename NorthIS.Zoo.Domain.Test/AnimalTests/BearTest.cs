﻿using System;
using Domain.Animals;
using Domain.Animals.Mammals;
using Domain.Food;
using Domain.Food.MeatFood;
using Domain.Food.PlantFood;
using NUnit.Framework;

namespace NorthIS.Zoo.Domain.Test.AnimalTests
{
	/// <summary>
	/// Проверка работы класса Bear.
	/// </summary>
	[TestFixture]
	internal class BearTest
	{
		#region Data
		#region Static
		/// <summary>
		/// Различные параметры для инициализации объектов животных.
		/// </summary>
		private static readonly AnimalParameters Parameters1 = new AnimalParameters(0, "", 0, 0);
		private static readonly AnimalParameters Parameters2 = new AnimalParameters(10, "Abigale", 1000, 200);
		private static readonly AnimalParameters Parameters3 = new AnimalParameters(-90, "Bertram", 0, 120);
		private static readonly AnimalParameters Parameters4 = new AnimalParameters(35, "Ciri", -15, 250);
		private static readonly AnimalParameters Parameters5 = new AnimalParameters(0, "Damien", 21, -3);
		private static readonly AnimalParameters Parameters6 = new AnimalParameters(-1, "Edward", -1, -1);
		/// <summary>
		/// Источник различных экземпляров объектов Bear для тестов.
		/// </summary>
		private static readonly object[] Bears =
		{
			new Bear(null, "Проверка с 'null' в параметрах"),
			new Bear(Parameters1, "Проверка с нулевыми параметрами"),
			new Bear(Parameters2, "Проверка на обычных параметрах"),
			new Bear(Parameters3, "Проверка на первый отрицательный параметр"),
			new Bear(Parameters4, "Проверка на третий отрицательный параметр"),
			new Bear(Parameters5, "Проверка на четвертый отрицательный параметр"),
			new Bear(Parameters6, "Проверка на все отрицательные числовые параметры")
		};
		/// <summary>
		/// Источник различных экземпляров объектов Food для тестов.
		/// </summary>
		private static readonly object[] FoodObjects =
		{
			new Apple(1),
			new Beef(1),
			new Carrot(1),
			new Chicken(1),
			new Fish(1),
			new Grain(1),
			new Grass(1)
		};
		#endregion

		#region Fields
		private readonly Type _referenceType = typeof(Bear);
		#endregion
		#endregion

		#region Public
		/// <summary>
		/// Проверяем инициализацию экземпляров класса <see cref="Bear" /> с различными видами параметров.
		/// </summary>
		/// <param name="bearTest"></param>
		[TestCaseSource(nameof(Bears))]
		[Description("Проверка на корректность задания типа животного при инициализации")]
		public void Type_Is_Correct(Animal bearTest)
		{
			Assert.That(bearTest, Is.TypeOf(_referenceType));
		}

		[TestCaseSource(nameof(Bears))]
		[Description("Проверка на создаваемость объектов класса Bear с разными параметрами")]
		public void Has_Values(Animal bearTest)
		{
			Assert.That(bearTest, Is.Not.Null);
		}

		[TestCaseSource(nameof(FoodObjects))]
		[Description("Проверка на корректность работы методов FoodEat и BecameHungry")]
		public void FoodEat_Is_Correct(Food foodTest)
		{
			Animal bearTest = new Bear(Parameters2, "Eats a lot");
			bearTest.FoodEat(foodTest);
			// Проверка на корректные задания значений после съедения.
			Assert.That(bearTest.CaloriesCurrent.Calories, Is.EqualTo(foodTest.CalculateCalories().Calories));
			Assert.That(bearTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(foodTest.CalculateCalories().Carbohydrates));
			Assert.That(bearTest.CaloriesCurrent.Fats, Is.EqualTo(foodTest.CalculateCalories().Fats));
			Assert.That(bearTest.CaloriesCurrent.Proteins, Is.EqualTo(foodTest.CalculateCalories().Proteins));
			// Проверка на то, что CaloriesCurrent задается не большим чем CaloriesNeed.
			Assert.That(bearTest.CaloriesCurrent.Calories, Is.LessThanOrEqualTo(bearTest.CaloriesNeed.Calories));
			Assert.That(bearTest.CaloriesCurrent.Carbohydrates, Is.LessThanOrEqualTo(bearTest.CaloriesNeed.Carbohydrates));
			Assert.That(bearTest.CaloriesCurrent.Fats, Is.LessThanOrEqualTo(bearTest.CaloriesNeed.Fats));
			Assert.That(bearTest.CaloriesCurrent.Proteins, Is.LessThanOrEqualTo(bearTest.CaloriesNeed.Proteins));
			// Проверка на корректное обнуление при вызове BecameHungry.
			bearTest.BecameHungry();
			Assert.That(bearTest.CaloriesCurrent.Calories, Is.EqualTo(0));
			Assert.That(bearTest.CaloriesCurrent.Carbohydrates, Is.EqualTo(0));
			Assert.That(bearTest.CaloriesCurrent.Fats, Is.EqualTo(0));
			Assert.That(bearTest.CaloriesCurrent.Proteins, Is.EqualTo(0));
		}

		#endregion
	}
}

