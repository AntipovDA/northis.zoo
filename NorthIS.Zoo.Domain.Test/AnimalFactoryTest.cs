﻿using System;
using Domain;
using Domain.Animals;
using Domain.Animals.Birds;
using Domain.Animals.Mammals;
using Domain.Animals.Reptiles;
using Domain.Factory;
using NUnit.Framework;

namespace NorthIS.Zoo.Domain.Test
{
	/// <summary>
	/// Проверка работоспособности фабрики животных.
	/// </summary>
	[TestFixture]
	internal class AnimalFactoryTest
	{
		#region Data
		#region Static
		private static readonly AnimalParameters AnimalOne = new AnimalParameters(1, "Alex", 5, 10);
		private static readonly AnimalParameters AnimalTri = new AnimalParameters(123, "AFBV", 781, 642);
		private static readonly AnimalParameters AnimalTwo = new AnimalParameters(10, "Boris", 21, 33);
		#endregion

		#region Fields
		private readonly AnimalFactory _factory = new AnimalFactory();
		#endregion
		#endregion

		#region Test methods
		/// <summary>
		/// Тест на наличие калорийности при создании животного разных видов.
		/// </summary>
		/// <param name="type"></param>
		[Test]
		public void Has_Calories_Value([Values(AnimalTypes.Python,
										   AnimalTypes.Crocodile,
										   AnimalTypes.Varan,
										   AnimalTypes.Eagle,
										   AnimalTypes.Duck,
										   AnimalTypes.Swan,
										   AnimalTypes.Lion,
										   AnimalTypes.Antelope,
										   AnimalTypes.Rabbit,
										   AnimalTypes.Bear)]
									   AnimalTypes type)
		{
			var testAnimal = _factory.CreateAnimal(type, AnimalOne, "");
			Assert.IsNotNull(testAnimal.CaloriesNeed);
		}

		/// <summary>
		/// Тест правильности создания животных.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="referenceType"></param>
		[Test]
		[Sequential]
		public void Is_Creating_Right_Animal([Values(AnimalTypes.Python,
												 AnimalTypes.Crocodile,
												 AnimalTypes.Varan,
												 AnimalTypes.Eagle,
												 AnimalTypes.Duck,
												 AnimalTypes.Swan,
												 AnimalTypes.Lion,
												 AnimalTypes.Antelope,
												 AnimalTypes.Rabbit,
												 AnimalTypes.Bear)]
											 AnimalTypes type,
											 [Values(typeof(Python),
												 typeof(Crocodile),
												 typeof(Varan),
												 typeof(Eagle),
												 typeof(Duck),
												 typeof(Swan),
												 typeof(Lion),
												 typeof(Antelope),
												 typeof(Rabbit),
												 typeof(Bear))]
											 Type referenceType)
		{
			var testAnimal = _factory.CreateAnimal(type, AnimalTwo, "TestDescription");
			Assert.IsNotNull(testAnimal);
			Assert.IsInstanceOf(referenceType, testAnimal);
			Assert.AreEqual(testAnimal.GetName(), "Boris");
			Assert.IsNotEmpty(testAnimal.Ration);
			Assert.AreEqual(testAnimal.Description, "TestDescription");
		}

		/// <summary>
		/// Тест на создание животных с разными параметрами.
		/// </summary>
		/// <param name="type"></param>
		[Test]
		public void Test_Factory_With_Different_properties([Values(AnimalTypes.Python,
															   AnimalTypes.Crocodile,
															   AnimalTypes.Varan,
															   AnimalTypes.Eagle,
															   AnimalTypes.Duck,
															   AnimalTypes.Swan,
															   AnimalTypes.Lion,
															   AnimalTypes.Antelope,
															   AnimalTypes.Rabbit,
															   AnimalTypes.Bear)]
														   AnimalTypes type)
		{
			var testAnimal = _factory.CreateAnimal(type, AnimalOne, "");
			Assert.IsNotNull(testAnimal);
			Assert.IsNotEmpty(testAnimal.Ration);
			Assert.AreEqual(testAnimal.GetName(), "Alex");
			testAnimal = _factory.CreateAnimal(type, AnimalTwo, "");
			Assert.IsNotNull(testAnimal);
			Assert.IsNotEmpty(testAnimal.Ration);
			Assert.AreEqual(testAnimal.GetName(), "Boris");
			testAnimal = _factory.CreateAnimal(type, AnimalTri, "");
			Assert.IsNotNull(testAnimal);
			Assert.IsNotEmpty(testAnimal.Ration);
		}
		#endregion
	}
}
